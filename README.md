# Python CSV Export Tool

### Python script used to load data from a file and export it in CSV format.
### Loaded data must meet the following criteria:
## yyy-mm-dd hh:ss value1 value2

### where yyyy - year , mm - month , dd - day , hh:ss - hour:secons , value1 and value2 - two numerical values

### Used for converting strored temperature and humidity data in log files into csv files.

## Iulian Cenusa , 2019

